# CT-Registration

Methods to perform CT Robot registration 

    1. Point based registration

        - Design inspired by [Faro Calibration](https://youtu.be/isbDl9hGdXk)

        - The position of the calibration ball attached to the end-effector can be cross checked with the laser attached to the     end_effector

        - The jig can have a female socket and it is easier to attach when compared to spherical balls

        - The rigidity of the spherical ball attached to the end_effector has to be checked.  

    2.  

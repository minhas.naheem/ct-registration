1. Parameters of CT volume:

    -CT increment:
        Slice Increment refers to the movement of the table/scanner for scanning the next slice (varying from 1 mm to 4 mm).

    -Slice width:
        If slice width and the slice increment are equal then that means the information is contigous. 
        if slice width is 2mm and increment is 1mm, the overlap is 50%

    An ideal setting is 1 mm increment and 3mm width
Note: In maxio based procedures the gantry tilt is kept to be zero. 
2. Co-ordinate system of CT:
    - CT global coordinate system origin lies in the isocenter of the gantry. Lets call it the global coordinate system of the Ct
    - The CT volume coordinate system is can be referenced to the glocal coordinate system by observing the translation of the bed along the z axis. The CT volume and the CT gantry coordinate system will only differ in the z axis coordinate and the value is directly decodable from the cradle offset value.
    - The minimum resolution one can acheive in the z axis movement is 0.5 mm
3. Calibration of the Robot and the CT:





Calibration Check:
1. The Instacheck: The instacheck involes the tilt sensor values and the camera marker alignment in the robots base
2. Table point check: In the robots home position the laser has to point the table marker and the alignment is checked to resume the procedure
3. The 45 degree angulations are checked in all four quadrants using a sponge phantom.This is done by capturing a CT of the phantom post insertion.

 
